# A file used for quick/informal testing during development
import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../truffle_hog")
from yf_lookup import YFTradingPeriod, YFTradingInterval, YFFinancialPeriod, YFFinancialInterval, YFLookup
from asset_features import AssetData
import asset_filter
import asset_calc

lookup = YFLookup(
    YFTradingPeriod.FIVE_YEARS, YFTradingInterval.ONE_MONTH, YFFinancialPeriod.MAX, YFFinancialInterval.QUARTERLY)

print('\n\nGET DATA TEST')
msft = lookup.lookup_ticker('MSFT')
print('\nMSFT INFO')
print(msft.info)
print('\nMSFT TRADING HISTORY')
print(msft.trading_history)
print('\nMSFT FINANCIAL HISTORY')
print(msft.financial_history)
print(asset_calc.get_data(msft, [AssetData.SYMBOL, AssetData.SHARE_PRICE, AssetData.PRICE_OPEN, AssetData.NET_INCOME]))
print(asset_calc.get_historic_data(msft, [AssetData.PRICE_OPEN, AssetData.NET_INCOME]))
print(asset_calc.get_historic_data(msft, []))

print('\n\nFILTER TEST')
trends = {}

symbols = ['ROK', 'DE', 'JOUT', 'DHT', 'SONY', 'TRMB']
assets = [ lookup.lookup_ticker(symbol) for symbol in symbols ]
print('\nUNFILTERED: ' + str(symbols))

def test_filter_expression(filter_expression, filter_label):
    test_filter = asset_filter.compile(filter_expression, trends)
    filtered_assets = list(filter(test_filter, assets))
    filtered_symbols = [ asset_calc.get_data(asset, (AssetData.SYMBOL,))[AssetData.SYMBOL] for asset in filtered_assets ]
    print(filter_label + ': ' + str(filtered_symbols))

test_filter_expression('accept', 'ACCEPT ALL')
test_filter_expression('reject', 'REJECT ALL')
test_filter_expression('SYMBOL match "D.*"', 'SYMBOLS START WITH D')
test_filter_expression('MARKET_CAP above 10e9', 'MARKET CAP ABOVE 10 BILLION')
test_filter_expression('PEG_RATIO not above (2)', 'PEG LESS THAN OR EQUAL TO 2')
