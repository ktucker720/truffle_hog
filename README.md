Truffle Hog is a tool for finding and filtering stocks and other assets. It is
presently under development but includes working functionality for ticker lookup
and filtering. Informal testing and demo of current functionality can be found
in the test directory.