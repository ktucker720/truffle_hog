from yf_asset import YFAsset
from lookup_features import LookupTicker

# How far back to sample trading data
class YFTradingPeriod:
    ONE_DAY = '1d'
    FIVE_DAYS = '5d'
    ONE_MONTH = '1mo'
    THREE_MONTHS = '3mo'
    SIX_MONTHS = '6mo'
    ONE_YEAR = '1y'
    TWO_YEARS = '2y'
    FIVE_YEARS = '5y'
    TEN_YEARS = '10y'
    YEAR_TO_DATE = 'ytd'
    MAX = 'max'

# The sample interval of the trading data
# Note: Intraday data cannot extend last 60 days
class YFTradingInterval:
    ONE_MINUTE = '1m'
    TWO_MINUTES = '2m'
    FIVE_MINUTES = '5m'
    FIFTEEN_MINUTES = '15m'
    THIRTY_MINUTES = '30m'
    SIXTY_MINUTES = '60m'
    NINETY_MINUTES = '90m'
    ONE_HOUR = '1h'
    ONE_DAY = '1d'
    FIVE_DAYS = '5d'
    ONE_WEEK = '1wk'
    ONE_MONTH = '1mo'
    THREE_MONTH = '3mo'

# How far back to sample financial data
# Note: yfinance provides very little financial history so the only
# practical option is to get it all
class YFFinancialPeriod:
    MAX = 'max'

# The sample interval of the financial data
class YFFinancialInterval:
    QUARTERLY = 'quarterly'
    YEARLY = 'yearly'

class YFLookup(LookupTicker):

    def __init__(self,
    trading_period=YFTradingPeriod.FIVE_YEARS,
    trading_interval=YFTradingInterval.ONE_MONTH,
    financial_period=YFFinancialPeriod.MAX,
    financial_interval=YFFinancialInterval.YEARLY):
        self._trading_period = trading_period
        self._trading_interval = trading_interval
        self._financial_period = financial_period
        self._financial_interval = financial_interval

    def lookup_ticker(self, ticker):
        return YFAsset(
            ticker, self._trading_period, self._trading_interval,
            self._financial_period, self._financial_interval)
