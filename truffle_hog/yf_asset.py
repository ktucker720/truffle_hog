from functools import reduce
import pandas as pd
import yfinance as yf
from asset_features import HISTORY_INDEX_NAME, AssetData, AssetInfo, AssetTradingHistory, AssetFinancialHistory

class YFAsset(AssetInfo, AssetTradingHistory, AssetFinancialHistory):

    def __init__(self, symbol, trading_period, trading_interval,
    financial_period, financial_interval):
        self._yfticker = yf.Ticker(symbol)
        self._trading_period = trading_period
        self._trading_interval = trading_interval
        self._financial_period = financial_period
        self._financial_interval = financial_interval
        self._info = None
        self._trading_history = None
        self._financial_history = None

    @property
    def info(self):
        self._load_info()
        return self._info

    @property
    def trading_history(self):
        self._load_trading_history()
        return self._trading_history

    @property
    def financial_history(self):
        self._load_financial_history()
        return self._financial_history

    def _load_info(self):
        if self._info is None:
            raw_info = self._yfticker.info

            # Select relevant info and re-label it (if it exists)
            info_map = {
                'symbol': AssetData.SYMBOL,
                'longName': AssetData.NAME,
                'sector': AssetData.SECTOR,
                'industry': AssetData.INDUSTRY,
                'country': AssetData.COUNTRY,
                'longBusinessSummary': AssetData.SUMMARY,
                'marketCap': AssetData.MARKET_CAP,
                'regularMarketPrice': AssetData.SHARE_PRICE,
                'trailingPE': AssetData.TRAILING_PE_RATIO,
                'forwardPE': AssetData.FORWARD_PE_RATIO,
                'pegRatio': AssetData.PEG_RATIO,
                'priceToSalesTrailing12Months': AssetData.PS_RATIO,
                'priceToBook': AssetData.PB_RATIO,
                'trailingAnnualDividendYield': AssetData.TRAILING_DIVIDEND_YIELD,
                'dividendYield': AssetData.FORWARD_DIVIDEND_YIELD,
                'payoutRatio': AssetData.PAYOUT_RATIO
                }
            info = { info_map[key]: raw_info.get(key) for key in info_map.keys() }
            info = { key: value for key, value in info.items() if value is not None }

            self._info = info

    def _load_trading_history(self):
        if self._trading_history is None:
            raw_history = self._yfticker.history(
                period=self._trading_period, interval=self._trading_interval)

            # Select relevant columns (if they exist)
            columns_map = {
                'Open': AssetData.PRICE_OPEN,
                'High': AssetData.PRICE_HIGH,
                'Low': AssetData.PRICE_LOW,
                'Close': AssetData.PRICE_CLOSE,
                'Volume': AssetData.TRADING_VOLUME
                }
            history = raw_history[raw_history.columns.intersection(columns_map.keys())].copy()

            # Re-label columns (if they exist)
            history.rename(columns=columns_map, inplace=True)

            # Change the row label
            history.index.name = HISTORY_INDEX_NAME

            # Remove rows with invalid data
            history.dropna(inplace=True)

            self._trading_history = history

    def _load_financial_history(self):
        if self._financial_history is None:
            raw_income = self._yfticker.get_financials(
                freq=self._financial_interval).transpose()
            raw_balance_sheet = self._yfticker.get_balancesheet(
                freq=self._financial_interval).transpose()
            raw_cashflow = self._yfticker.get_cashflow(
                freq=self._financial_interval).transpose()
            raw_history = reduce(
                lambda left, right: pd.merge(left, right, how='outer', left_index=True, right_index=True, suffixes=('', '_y')), 
                [raw_income, raw_balance_sheet, raw_cashflow])

            # Select relevant columns (if they exist)
            columns_map = {
                'Total Revenue': AssetData.TOTAL_REVENUE,
                'Cost Of Revenue': AssetData.COST_OF_REVENUE,
                'Gross Profit': AssetData.GROSS_INCOME,
                'Total Operating Expenses': AssetData.TOTAL_OPERATING_EXPENSE,
                'Selling General Administrative': AssetData.SGA_EXPENSE,
                'Research Development': AssetData.RD_EXPENSE,
                'Depreciation': AssetData.DEPR_AMOR_DEPL_EXPENSE,
                'Operating Income': AssetData.OPERATING_INCOME,
                'Net Income': AssetData.NET_INCOME,
                'Total Assets': AssetData.TOTAL_ASSETS,
                'Total Current Assets': AssetData.CURRENT_ASSETS,
                'Total Liab': AssetData.TOTAL_LIABILITIES,
                'Total Current Liabilities': AssetData.CURRENT_LIABILITIES,
                'Total Cash From Operating Activities': AssetData.OPERATING_CASH_FLOW,
                'Total Cashflows From Investing Activities': AssetData.INVESTING_CASH_FLOW,
                'Total Cash From Financing Activities': AssetData.FINANCING_CASH_FLOW
                }
            history = raw_history[raw_history.columns.intersection(columns_map.keys())].copy()

            # Re-label columns (if they exist)
            history.rename(columns=columns_map, inplace=True)

            # Change the row label
            history.index.name = HISTORY_INDEX_NAME

            # Remove columns with invalid data
            history.dropna(axis='columns', inplace=True)

            self._financial_history = history
