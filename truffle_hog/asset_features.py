from abc import ABC, abstractmethod
from enum import Enum

# Row index name for trading/financial history data
HISTORY_INDEX_NAME = 'Time'

# Types of asset data items
class AssetDataType(Enum):
    UNKNOWN = 0
    TEXT = 1
    CURRENCY = 2
    RATIO = 3
    COUNT = 4

STRING_LIKE_ASSET_DATA_TYPES = (AssetDataType.TEXT,)
NUMBER_LIKE_ASSET_DATA_TYPES = (AssetDataType.CURRENCY, AssetDataType.RATIO, AssetDataType.COUNT)

# Asset data item identification
class AssetData:

    # Types of general information and individual statistics

    # Symbol identifying the asset on a particular market
    SYMBOL = 'SYMBOL'

    # Name of the asset (e.g. company name, fund name, etc.)
    NAME = 'NAME'

    # Economic sector of the asset
    SECTOR = 'SECTOR'

    # Economic industry of the asset
    INDUSTRY = 'INDUSTRY'

    # Country of the asset
    COUNTRY = 'COUNTRY'

    # Description for the asset
    SUMMARY = 'SUMMARY'

    # Market price for the entire asset
    MARKET_CAP = 'MARKET_CAP'

    # Market price for one share of the asset
    SHARE_PRICE = 'SHARE_PRICE'

    # Price to past year's earnings ratio
    TRAILING_PE_RATIO = 'TRAILING_PE_RATIO'

    # Price to forcasted earnings ratio
    FORWARD_PE_RATIO = 'FORWARD_PE_RATIO'

    # Price to earnings to growth ratio
    PEG_RATIO = 'PEG_RATIO'

    # Price to past year's sales ratio
    PS_RATIO = 'PS_RATIO'

    # Price to most recent quarter book value ratio
    PB_RATIO = 'PB_RATIO'

    # Past year's dividend to price ratio
    TRAILING_DIVIDEND_YIELD = 'TRAILING_DIVIDEND_YIELD'

    # Forecasted year's dividend to price ratio
    FORWARD_DIVIDEND_YIELD = 'FORWARD_DIVIDEND_YIELD'

    # Dividend payout to earnings ratio
    PAYOUT_RATIO = 'PAYOUT_RATIO'


    # Types of trading history data

    # The share price at the beginning of an interval
    PRICE_OPEN = 'PRICE_OPEN'

    # The highest share price during an interval
    PRICE_HIGH = 'PRICE_HIGH'

    # The lowest share price during an interval
    PRICE_LOW = 'PRICE_LOW'

    # The share price at the end of an interval
    PRICE_CLOSE = 'PRICE_CLOSE'

    # The number of shares traded during an interval
    TRADING_VOLUME = 'TRADING_VOLUME'


    # Types of financial history data

    # Income statement data

    # All revenue
    TOTAL_REVENUE = 'TOTAL_REVENUE'

    # The direct costs of generating revenue (e.g. raw materials costs, production employee wages, etc.)
    COST_OF_REVENUE = 'COST_OF_REVENUE'

    # Income/profit after deducting cost of revenue from total revenue
    GROSS_INCOME = 'GROSS_INCOME'

    # All operating costs (includes cost of revenue, SG&A, R&D, depreciation, amortization, depletion, etc.)
    TOTAL_OPERATING_EXPENSE = 'TOTAL_OPERATING_EXPENSE'

    # Selling, General, and Administrative costs (i.e. indirect costs like IT department)
    SGA_EXPENSE = 'SGA_EXPENSE'
    
    # Research and development costs
    RD_EXPENSE = 'RD_EXPENSE'

    # Depreciation is lost value of tangible assets (e.g. a machine as it ages),
    # amortization is lost value of intangible assets (e.g. a patent as it approaches expiration),
    # and depletion is lost value of natural resources (e.g. an oil field as oil is extracted from it).
    # Capital expenditures are not immediately reported on income sheets but instead are spread
    # over several years as a depreciation expense as the purchased asset is used up over its useful
    # lifetime.
    DEPR_AMOR_DEPL_EXPENSE = 'DEPR_AMOR_DEPL_EXPENSE'

    # Income/profit after deducting operating expenses from total revenue
    OPERATING_INCOME = 'OPERATING_INCOME'

    # Income/profit after deducting non-operating expenses (e.g. interest, taxes, etc.) from operating income
    # Note: also known as earnings or the 'bottom line'
    NET_INCOME = 'NET_INCOME'

    
    # Balance sheet data

    # All assets
    TOTAL_ASSETS = 'TOTAL_ASSETS'

    # Short term assets (e.g. cash, accounts receivable, etc.)
    CURRENT_ASSETS = 'CURRENT_ASSETS'

    # All debts
    TOTAL_LIABILITIES = 'TOTAL_LIABILITIES'

    # Short term obligations (e.g. short term debt, accounts payable, etc.)
    CURRENT_LIABILITIES = 'CURRENT_LIABILITIES'


    # Cash flow statement data

    # Cash generated from normal business operations
    OPERATING_CASH_FLOW = 'OPERATING_CASH_FLOW'

    INVESTING_CASH_FLOW = 'INVESTING_CASH_FLOW'

    FINANCING_CASH_FLOW = 'FINANCING_CASH_FLOW'


    # Data that is typically derived from calculations

    # Operating Income / Total Revenue
    OPERATING_MARGIN = 'OPERATING_MARGIN'

    # Current Assets / Current Liabilities
    CURRENT_RATIO = 'CURRENT_RATIO'

    # Type specifier lookup for AssetData items
    _ASSET_DATA_TYPE_SPECIFIERS = {
        SYMBOL: AssetDataType.TEXT,
        NAME: AssetDataType.TEXT,
        SECTOR: AssetDataType.TEXT,
        INDUSTRY: AssetDataType.TEXT,
        COUNTRY: AssetDataType.TEXT,
        SUMMARY: AssetDataType.TEXT,
        MARKET_CAP: AssetDataType.CURRENCY,
        SHARE_PRICE: AssetDataType.CURRENCY,
        TRAILING_PE_RATIO: AssetDataType.RATIO,
        FORWARD_PE_RATIO: AssetDataType.RATIO,
        PEG_RATIO: AssetDataType.RATIO,
        PS_RATIO: AssetDataType.RATIO,
        PB_RATIO: AssetDataType.RATIO,
        TRAILING_DIVIDEND_YIELD: AssetDataType.RATIO,
        FORWARD_DIVIDEND_YIELD: AssetDataType.RATIO,
        PAYOUT_RATIO: AssetDataType.RATIO,
        PRICE_OPEN: AssetDataType.CURRENCY,
        PRICE_HIGH: AssetDataType.CURRENCY,
        PRICE_LOW: AssetDataType.CURRENCY,
        PRICE_CLOSE: AssetDataType.CURRENCY,
        TRADING_VOLUME: AssetDataType.COUNT,
        TOTAL_REVENUE: AssetDataType.CURRENCY,
        COST_OF_REVENUE: AssetDataType.CURRENCY,
        GROSS_INCOME: AssetDataType.CURRENCY,
        TOTAL_OPERATING_EXPENSE: AssetDataType.CURRENCY,
        SGA_EXPENSE: AssetDataType.CURRENCY,
        RD_EXPENSE: AssetDataType.CURRENCY,
        DEPR_AMOR_DEPL_EXPENSE: AssetDataType.CURRENCY,
        OPERATING_INCOME: AssetDataType.CURRENCY,
        NET_INCOME: AssetDataType.CURRENCY,
        TOTAL_ASSETS: AssetDataType.CURRENCY,
        CURRENT_ASSETS: AssetDataType.CURRENCY,
        TOTAL_LIABILITIES: AssetDataType.CURRENCY,
        CURRENT_LIABILITIES: AssetDataType.CURRENCY,
        OPERATING_CASH_FLOW: AssetDataType.CURRENCY,
        INVESTING_CASH_FLOW: AssetDataType.CURRENCY,
        FINANCING_CASH_FLOW: AssetDataType.CURRENCY,
        OPERATING_MARGIN: AssetDataType.RATIO,
        CURRENT_RATIO: AssetDataType.RATIO,
    }

    def get_type(data_identifier):
        return AssetData._ASSET_DATA_TYPE_SPECIFIERS.get(data_identifier, AssetDataType.UNKNOWN)


# General information and individual statistics
# Note: this information is returned as a dictionary with asset data type keys
class AssetInfo(ABC):

    @property
    @abstractmethod
    def info(self):
        pass

# Trading history may include data like share price and trading volume
# Note: this information is returned as a pandas DataFrame with datetime row indices and asset data type labeled columns
class AssetTradingHistory(ABC):

    @property
    @abstractmethod
    def trading_history(self):
        pass

# Financial history may include data from income statements, balance sheets, and cash flow statements
# Note: this information is returned as a pandas DataFrame with datetime row indices and asset data type labeled columns
class AssetFinancialHistory(ABC):

    @property
    @abstractmethod
    def financial_history(self):
        pass
