from enum import Enum
import numpy as np
import pandas as pd
from asset_features import AssetData, AssetInfo, AssetTradingHistory, AssetFinancialHistory

# Try to extract and derive data from assets

# How to handle failure to calculate a particular data item
class FailureMode(Enum):
    # Stop calculating and throw a DataFailure exception
    THROW = 0

    # Stop calculating and return None
    RETURN_NONE = 1

    # Ommit the data item and continue calculating the other data items
    OMMIT = 2

# An exception that may be thrown if a calculation fails
class DataFailure(Exception):
    pass

# Get data, only selecting the newest data if time-based
# (returns a dictionary with asset data type label keys)
def get_data(asset, data_identifiers, failure_mode=FailureMode.THROW):
    data = {}
    info = None
    trading_latest = None
    financial_latest = None

    for data_identifier in data_identifiers:
        
        if info is None and isinstance(asset, AssetInfo):
            info = asset.info

        if info is not None:
            item = info.get(data_identifier)
            if item is not None:
                data[data_identifier] = item
                continue
        
        if trading_latest is None and isinstance(asset, AssetTradingHistory):
            trading_history = asset.trading_history
            if trading_history.shape[0] == 0 or trading_history.shape[1] == 0:
                trading_latest = pd.Series()
            else:
                trading_latest = trading_history.loc[trading_history.index.max()]

        if trading_latest is not None:
            item = trading_latest.get(data_identifier)
            if item is not None:
                data[data_identifier] = item
                continue
        
        if financial_latest is None and isinstance(asset, AssetFinancialHistory):
            financial_history = asset.financial_history
            if financial_history.shape[0] == 0 or financial_history.shape[1] == 0:
                financial_latest = pd.Series()
            else:
                financial_latest = financial_history.loc[financial_history.index.max()]
                
        if financial_latest is not None:
            item = financial_latest.get(data_identifier)
            if item is not None:
                data[data_identifier] = item
                continue

        # Failed to find the data
        if failure_mode is FailureMode.THROW:
            raise DataFailure('No data "' + data_identifier + '"')
        elif failure_mode is FailureMode.RETURN_NONE:
            return None
    
    return data

# Get time-based data (returns a pandas DataFrame with datetime row
# indices and asset data type labeled columns). If data items from
# different data sets are selected, the merged data set will only
# contain data for the overlapping datetime range. Trading history
# timestamped rows are used and the latest financial data 
# at that timestamp is used.
def get_historic_data(asset, data_identifiers, failure_mode=FailureMode.THROW):
    data_identifiers_set = set(data_identifiers)

    trading = None
    if isinstance(asset, AssetTradingHistory):
        trading_history = asset.trading_history
        if trading_history.shape[0] > 0:
            trading = trading_history.loc[:, trading_history.columns.isin(data_identifiers_set)].sort_index(ascending=True)
            if trading.shape[1] == 0:
                trading = None

    financial = None
    if isinstance(asset, AssetFinancialHistory):
        financial_history = asset.financial_history
        if financial_history.shape[0] > 0: 
            financial = financial_history.loc[:, financial_history.columns.isin(data_identifiers_set)].sort_index(ascending=True)
            if financial.shape[1] == 0:
                financial = None

    if trading is not None and financial is not None:
        # Both trading data and financial data
        data = _merge_trading_financial(trading, financial)
    elif trading is not None:
        # Only trading data
        data = trading
    elif financial is not None:
        # Only financial data
        data = financial
    else:
        # No data (empty frame)
        data = pd.DataFrame()

    # Handle missing data
    data_columns_set = set(data.columns)
    if failure_mode is FailureMode.THROW:
        for data_identifier in data_identifiers_set:
            if data_identifier not in data_columns_set:
                raise DataFailure('No data "' + data_identifier + '"')
    elif failure_mode is FailureMode.RETURN_NONE:
        if data_identifiers_set != data_columns_set:
            return None

    return data

def _merge_trading_financial(trading, financial):
    # Add empty columns to the trading data frame for financial data
    data = trading.copy()
    for financial_column in financial.columns:
        data[financial_column] = np.nan

    # Get the first row of financial data (assumed to exist)
    financial_row_iter = financial.iterrows()
    financial_index_prev = None
    financial_row_prev = None
    financial_index, financial_row = next(financial_row_iter)
    
    # Fill in the new columns with financial data
    for data_index, data_row in data.iterrows():
        # Check for newer financial data if necessary
        if financial_index < data_index:
            # Get the next row of financial data if it exists
            try:
                financial_index_prev = financial_index
                financial_row_prev = financial_row
                financial_index, financial_row = next(financial_row_iter)
            except StopIteration:
                pass
    
        # If there is lagging financial data to associate with the trading data
        # then fill it in
        if financial_index_prev is not None:
            for financial_column in financial.columns:
                data_row[financial_column] = financial_row_prev[financial_column]

    # Remove rows from the merged data set with missing financial data (e.g. those
    # rows with trading history that is older than the oldest financial history)
    data.dropna(inplace=True)

    # If the data frame contains no data then replace it with an empty frame
    if data.shape[0] == 0 or data.shape[1] == 0:
        data = pd.DataFrame()

    return data
