from enum import Enum, auto
from numbers import Number
import re
import asset_calc
from asset_features import AssetDataType, STRING_LIKE_ASSET_DATA_TYPES, NUMBER_LIKE_ASSET_DATA_TYPES, AssetData

# An asset filter is used to screen out assets in a collection of assets based 
# on some condition. Asset filters can be combined. Apply filters to
# collections of assets using the Python built-in function, filter().

# An exception that is thrown if a filter operand is invalid
class BadOperand(Exception):
    pass


# An exception that is thrown if a filter expression is invalid
class BadExpression(Exception):
    pass


# The types of tokens in a filter expression
class _TokenType(Enum):
    OPEN_PAREN = auto()
    CLOSE_PAREN = auto()
    OPER_NOT = auto()
    OPER_AND = auto()
    OPER_OR = auto()
    OPER_MATCH = auto()
    OPER_ABOVE = auto()
    OPER_BELOW = auto()
    OPER_TREND = auto()
    CONST_STRING = auto()
    CONST_NUMBER = auto()
    IDENTIFIER = auto()


# The syntactical elements of a filter expression
class _SyntaxType(Enum):
    # Filter operations (all evaluate to a filter operand)
    NOT_OPERATION = auto()
    AND_OPERATION = auto()
    OR_OPERATION = auto()

    # Filter operands
    ACCEPT_FILTER = auto()
    REJECT_FILTER = auto()

    # Data operations (all evaluate to a filter operand)
    MATCH_OPERATION = auto()
    ABOVE_OPERATION = auto()
    BELOW_OPERATION = auto()
    TREND_OPERATION = auto()

    # Data operands (which are only used in data operations)
    DATA_IDENTIFIER = auto()
    STRING_LITERAL = auto()
    NUMBER_LITERAL = auto()
    TREND_IDENTIFIER = auto()


FILTER_EXPRESSIONS = set((
    _SyntaxType.NOT_OPERATION, _SyntaxType.AND_OPERATION, 
    _SyntaxType.OR_OPERATION, _SyntaxType.ACCEPT_FILTER,
    _SyntaxType.REJECT_FILTER, _SyntaxType.MATCH_OPERATION,
    _SyntaxType.ABOVE_OPERATION, _SyntaxType.BELOW_OPERATION,
    _SyntaxType.TREND_OPERATION))


class _SyntaxTreeNode:
    def __init__(self, syntax_type, value=None, left=None, right=None):
        self.syntax_type = syntax_type
        self.value = value
        self.left = left
        self.right = right


# Create an asset filter from a filter expression string
# Note: trends is a dictionary mapping trend identifier strings to trend functions
# Example - Pass through assets with a symbol starting with D and a market cap larger than 1 billion
# 'SYMBOL match "D.*" and MARKET_CAP above 1000000000'
def compile(filter_expression, trends=None):
    if trends is None:
        trends = {}

    # Get list of token strings
    tokens = _tokenize_expression(filter_expression)

    # Convert tokens to token_type, value pairs
    classified_tokens = _classify_tokens(tokens)

    # Create a syntax tree by combining tokens into subexpressions and
    # determining order of operations
    syntax_tree = _create_syntax_tree(classified_tokens)

    # Verify the expression evaluates to a filter
    if syntax_tree.syntax_type not in FILTER_EXPRESSIONS:
        raise BadExpression('Filter expression is incomplete')

    # Translate the syntax tree into a Python callable function
    compiled_filter = _to_function(syntax_tree, trends)

    return compiled_filter


# The logical NOT of the asset filter
def logical_not(asset_filter):
    def new_filter(asset):
        return not asset_filter(asset)
    return new_filter


# The logical AND of the asset filters
def logical_and(asset_filters):
    def combined_filter(asset):
        for f in asset_filters:
            if not f(asset):
                return False
        return True
    return combined_filter


# The logical OR of the asset filters
def logical_or(asset_filters):
    def combined_filter(asset):
        for f in asset_filters:
            if f(asset):
                return True
        return False
    return combined_filter


# An asset filter that will always pass through
def accept(asset):
    return True


# An asset filter that will always screen out
def reject(asset):
    return False


def create_match_filter(data_identifier, match):
    data_type = AssetData.get_type(data_identifier)
    if data_type in STRING_LIKE_ASSET_DATA_TYPES:
        if not isinstance(match, str):
            raise BadOperand('Data "' + str(match) + '" expected to be a string')
        
        regex_match = re.compile(match)

        def string_match_filter(asset):
            data = asset_calc.get_data(asset, (data_identifier,), asset_calc.FailureMode.RETURN_NONE)
            #TEMP
            x = regex_match.match(data[data_identifier])
            return data is not None and bool(regex_match.match(data[data_identifier]))

        return string_match_filter
    elif data_type in NUMBER_LIKE_ASSET_DATA_TYPES:
        if not isinstance(match, Number):
            raise BadOperand('Data "' + str(match) + '" expected to be a number')
        
        def number_match_filter(asset):
            data = asset_calc.get_data(asset, (data_identifier,), asset_calc.FailureMode.RETURN_NONE)
            return data is not None and data[data_identifier] == match

        return number_match_filter
    else:
        raise BadOperand('Data "' + data_identifier + '" not supported by match operation')


def create_above_filter(data_identifier, above):
    return _create_above_below_filter(data_identifier, above, 'above')


def create_below_filter(data_identifier, below):
    return _create_above_below_filter(data_identifier, below, 'below')


def create_trend_filter(data_identifier, trend):
    data_type = AssetData.get_type(data_identifier)
    if data_type not in NUMBER_LIKE_ASSET_DATA_TYPES:
        raise BadOperand('Data "' + data_identifier + '" not supported by trend operation')
    if not callable(trend):
        raise BadOperand('Trend not valid')
    def trend_filter(asset):
            data = asset_calc.get_historic_data(asset, (data_identifier,), asset_calc.FailureMode.RETURN_NONE)
            return data is not None and trend(data[data_identifier])
    return trend_filter


def _create_above_below_filter(data_identifier, limit, filter_type):
    data_type = AssetData.get_type(data_identifier)
    if data_type in STRING_LIKE_ASSET_DATA_TYPES:
        if not isinstance(limit, str):
            raise BadOperand('Data "' + str(limit) + '" expected to be a string')
    elif data_type in NUMBER_LIKE_ASSET_DATA_TYPES:
        if not isinstance(limit, Number):
            raise BadOperand('Data "' + str(limit) + '" expected to be a number')
    else:
        raise BadOperand('Data "' + data_identifier + '" not supported by ' + filter_type + ' operation')

    if filter_type == 'above':
        def above_filter(asset):
            data = asset_calc.get_data(asset, (data_identifier,), asset_calc.FailureMode.RETURN_NONE)
            return data is not None and data[data_identifier] > limit
        return above_filter
    else:
        def below_filter(asset):
            data = asset_calc.get_data(asset, (data_identifier,), asset_calc.FailureMode.RETURN_NONE)
            return data is not None and data[data_identifier] < limit
        return below_filter


def _tokenize_expression(filter_expression):
    tokens = []

    # Parse the filter expression into tokens where
    # string literals are treated as one token and
    # each parenthesis is treated as one token
    token = ''
    in_quote = False
    escape = False
    for c_loc, c in enumerate(filter_expression):
        # Verify a previous escape character is followed by a valid 
        # escapable character
        if escape and c not in ('"', '\\'):
            raise BadExpression('Invalid escape at ' + str(c_loc))

        if c.isspace():
            if in_quote:
                token += c
            elif len(token) > 0:
                tokens.append(token)
                token = ''

        elif c == '"':
            if in_quote:
                if escape:
                    escape = False
                else:
                    in_quote = False
            else:
                if len(token) > 0:
                    raise BadExpression('Missing space before quote at ' + str(c_loc + 1))
                in_quote = True
            token += c

        elif c == '\\':
            if not in_quote:
                raise BadExpression('Invalid escape at ' + str(c_loc + 1))
            if escape:
                token += c
            escape = not escape

        elif c == '(':
            if in_quote:
                token += c
            else:
                if len(token) > 0:
                    raise BadExpression('Missing space before parenthesis at ' + str(c_loc + 1))
                tokens.append(c)
                
        elif c == ')':
            if in_quote:
                token += c
            else:
                if len(token) > 0:
                    tokens.append(token)
                    token = ''
                tokens.append(c)

        else:
            token += c

    if in_quote:
        raise BadExpression('Missing closing quote')

    if len(token) > 0:  
        tokens.append(token)

    return tokens


def _classify_tokens(tokens):
    classified_tokens = []

    for token in tokens:
        value = None
        if token == '(':
            token_type = _TokenType.OPEN_PAREN
        elif token == ')':
            token_type = _TokenType.CLOSE_PAREN
        elif token == 'not':
            token_type = _TokenType.OPER_NOT
        elif token == 'and':
            token_type = _TokenType.OPER_AND
        elif token == 'or':
            token_type = _TokenType.OPER_OR
        elif token == 'match':
            token_type = _TokenType.OPER_MATCH
        elif token == 'above':
            token_type = _TokenType.OPER_ABOVE
        elif token == 'below':
            token_type = _TokenType.OPER_BELOW
        elif token == 'trend':
            token_type = _TokenType.OPER_TREND
        elif token[0] == '"':
            token_type = _TokenType.CONST_STRING
            value = token[1:-1]
        elif token[0].isdigit():
            token_type = _TokenType.CONST_NUMBER
            try:
                value = float(token)
            except ValueError:
                raise BadExpression('Invalid number "' + token + '"')
        else:
            # Must be an identifier

            # Verify the identifier uses valid characters
            for c in token:
                if not c.isalnum() and c != '_':
                    raise BadExpression('Invalid identifier "' + token + '"')
            
            token_type = _TokenType.IDENTIFIER
            value = token
        
        classified_tokens.append((token_type, value))

    return classified_tokens


def _create_syntax_tree(classified_tokens):

    if len(classified_tokens) == 0:
        raise BadExpression('Empty (sub)expression')

    # Parse following order of operations:
    # (1) parentheses (2) data operations (3) not operation (4) and operation (5) or operation

    # Convert classified tokens into syntax subtrees
    # Create complete subtrees recursively for tokens in parentheses and remove parentheses.
    # Create single node subtrees for other tokens where operands are leaf nodes and 
    # operators are internal nodes with children missing for now.
    syntax_subtrees = []
    paren_diff = 0
    open_index = None
    for index, classified_token in enumerate(classified_tokens):
        token_type, value = classified_token

        if token_type is _TokenType.OPEN_PAREN:
            paren_diff += 1
        elif token_type is _TokenType.CLOSE_PAREN:
            paren_diff -= 1
        
        if paren_diff < 0:
            raise BadExpression('Extra closing parenthesis')
        elif paren_diff > 0:
            if open_index is None:
                # The token is a level one opening parenthesis
                open_index = index
            # else - the token is in parentheses and will be handled recursively
        else:
            if open_index is None:
                # Token not enclosed in parentheses, convert tokens to nodes
                if token_type is _TokenType.OPER_NOT:
                    subtree = _SyntaxTreeNode(_SyntaxType.NOT_OPERATION)
                elif token_type is _TokenType.OPER_AND:
                    subtree = _SyntaxTreeNode(_SyntaxType.AND_OPERATION)
                elif token_type is _TokenType.OPER_OR:
                    subtree = _SyntaxTreeNode(_SyntaxType.OR_OPERATION)
                elif token_type is _TokenType.OPER_MATCH:
                    subtree = _SyntaxTreeNode(_SyntaxType.MATCH_OPERATION)
                elif token_type is _TokenType.OPER_ABOVE:
                    subtree = _SyntaxTreeNode(_SyntaxType.ABOVE_OPERATION)
                elif token_type is _TokenType.OPER_BELOW:
                    subtree = _SyntaxTreeNode(_SyntaxType.BELOW_OPERATION)
                elif token_type is _TokenType.OPER_TREND:
                    subtree = _SyntaxTreeNode(_SyntaxType.TREND_OPERATION)
                elif token_type is _TokenType.CONST_STRING:
                    subtree = _SyntaxTreeNode(_SyntaxType.STRING_LITERAL, value)
                elif token_type is _TokenType.CONST_NUMBER:
                    subtree = _SyntaxTreeNode(_SyntaxType.NUMBER_LITERAL, value)
                elif token_type is _TokenType.IDENTIFIER:
                    # The identifier is the accept filter if it is called 'accept'.
                    # The identifier is the reject filter if it is called 'reject'.
                    # The identifier is a trend identifier if preceeded by the
                    # trend operator. Otherwise it is a data identifier.
                    if value == 'accept':
                        subtree = _SyntaxTreeNode(_SyntaxType.ACCEPT_FILTER)
                    elif value == 'reject':
                        subtree = _SyntaxTreeNode(_SyntaxType.REJECT_FILTER)
                    elif index > 0 and classified_tokens[index - 1][0] is _TokenType.OPER_TREND:
                        subtree = _SyntaxTreeNode(_SyntaxType.TREND_IDENTIFIER, value)
                    else:
                        subtree = _SyntaxTreeNode(_SyntaxType.DATA_IDENTIFIER, value)
                else:
                    # Missing handling for a new token type if here
                    raise BadExpression('Bad token type')
            else:
                # The token is a closing parenthesis that matches up with the
                # previous level one opening parenthesis
                subtree = _create_syntax_tree(classified_tokens[open_index + 1 : index])
                open_index = None
            syntax_subtrees.append(subtree)

    if paren_diff > 0:
        raise BadExpression('Missing closing parenthesis')

    # Combine syntax subtrees into one syntax tree

    index = 0
    syntax_subtrees_len = len(syntax_subtrees)
    while index < syntax_subtrees_len:
        subtree = syntax_subtrees[index]
        # If the subtree is an incomplete data operation
        if subtree.syntax_type in (
            _SyntaxType.MATCH_OPERATION, _SyntaxType.ABOVE_OPERATION,
            _SyntaxType.BELOW_OPERATION, _SyntaxType.TREND_OPERATION) and subtree.left is None:
            if index == 0:
                raise BadExpression('Missing left operand for data operation')
            elif index >= syntax_subtrees_len - 1:
                raise BadExpression('Missing right operand for data operation')
            
            # Check if the data operation is negated (e.g. not above)
            negate = False
            left_index = index - 1
            left = syntax_subtrees[left_index]
            if left.syntax_type is _SyntaxType.NOT_OPERATION:
                if left_index == 0:
                    raise BadExpression('Missing left operand for data operation')
                negate = True
                left_index -= 1
                left = syntax_subtrees[left_index]

            if left.syntax_type is not _SyntaxType.DATA_IDENTIFIER:
                raise BadExpression('Left operand expected to be a data identifier')

            right_index = index + 1
            right = syntax_subtrees[right_index]
            if subtree.syntax_type is _SyntaxType.TREND_OPERATION:
                if right.syntax_type is not _SyntaxType.TREND_IDENTIFIER:
                    raise BadExpression('Right operand expected to be a trend identifier')
            else:
                if right.syntax_type not in (_SyntaxType.STRING_LITERAL, _SyntaxType.NUMBER_LITERAL):
                    raise BadExpression('Right operand expected to be a literal')

            # Make the operands children of the operation
            subtree.left = left
            subtree.right = right

            # Negate the operation if necessary
            if negate:
                subtree = _SyntaxTreeNode(_SyntaxType.NOT_OPERATION, right=subtree)

            # Replace the separate subtrees with the combined subtree in the 
            # list, adjusting the list index and length as necessary
            syntax_subtrees = syntax_subtrees[:left_index] + [subtree] + syntax_subtrees[right_index + 1:]
            index = left_index
            syntax_subtrees_len = len(syntax_subtrees)

        index += 1

    index = 0
    syntax_subtrees_len = len(syntax_subtrees)
    while index < syntax_subtrees_len:
        subtree = syntax_subtrees[index]
        # If the subtree is an incomplete not operation
        if subtree.syntax_type is _SyntaxType.NOT_OPERATION and subtree.right is None:
            if index >= syntax_subtrees_len - 1:
                raise BadExpression('Missing right operand for not operation')

            # Not can only be applied to filters
            right_index = index + 1
            right = syntax_subtrees[right_index]
            if right.syntax_type not in FILTER_EXPRESSIONS:
                raise BadExpression('Right operand expected to be a filter expression')

            if right.syntax_type is _SyntaxType.NOT_OPERATION:
                # This not and the following not cancel out
                middle = []
                if right.right is not None:
                    middle.append(right.right)
                syntax_subtrees = syntax_subtrees[:index] + middle + syntax_subtrees[right_index + 1:]
                index -= 1
            else:
                # If there is an incomplete and/or after the not then an operand is missing
                if right.syntax_type in (_SyntaxType.AND_OPERATION, _SyntaxType.OR_OPERATION) and right.left is None:
                    raise BadExpression('Missing right operand for not operation')

                # Make the operand the right child of the operation
                subtree.right = right

                # Replace the separate subtrees with the combined subtree in the list
                syntax_subtrees = syntax_subtrees[:index] + [subtree] + syntax_subtrees[right_index + 1:]
            
            # Adjust the list length as necessary
            syntax_subtrees_len = len(syntax_subtrees)

        index += 1

    index = 0
    syntax_subtrees_len = len(syntax_subtrees)
    while index < syntax_subtrees_len:
        subtree = syntax_subtrees[index]
        # If the subtree is an incomplete and operation
        if subtree.syntax_type is _SyntaxType.AND_OPERATION and subtree.left is None:
            if index == 0:
                raise BadExpression('Missing left operand for and operation')
            elif index >= syntax_subtrees_len - 1:
                raise BadExpression('Missing right operand for and operation')
            
            # And can only have filters as left operands
            left_index = index - 1
            left = syntax_subtrees[left_index]
            if left.syntax_type not in FILTER_EXPRESSIONS:
                raise BadExpression('Left operand expected to be a filter expression')

            # If there is an incomplete or before the and then an operand is missing for the or operation
            if left.syntax_type is _SyntaxType.OR_OPERATION and left.left is None:
                raise BadExpression('Missing right operand for or operation')

            # And can only have filters as right operands
            right_index = index + 1
            right = syntax_subtrees[right_index]
            if right.syntax_type not in FILTER_EXPRESSIONS:
                raise BadExpression('Right operand expected to be a filter expression')

            # If there is an incomplete and/or after the and then an operand is missing
            if right.syntax_type in (_SyntaxType.AND_OPERATION, _SyntaxType.OR_OPERATION) and right.left is None:
                raise BadExpression('Missing right operand for and operation')

            # Make the operands children of the operation
            subtree.left = left
            subtree.right = right

            # Replace the separate subtrees with the combined subtree in the 
            # list, adjusting the list index and length as necessary
            syntax_subtrees = syntax_subtrees[:left_index] + [subtree] + syntax_subtrees[right_index + 1:]
            index = left_index
            syntax_subtrees_len = len(syntax_subtrees)

        index += 1

    index = 0
    syntax_subtrees_len = len(syntax_subtrees)
    while index < syntax_subtrees_len:
        subtree = syntax_subtrees[index]
        # If the subtree is an incomplete or operation
        if subtree.syntax_type is _SyntaxType.OR_OPERATION and subtree.left is None:
            if index == 0:
                raise BadExpression('Missing left operand for or operation')
            elif index >= syntax_subtrees_len - 1:
                raise BadExpression('Missing right operand for or operation')
            
            # Or can only have filters as left operands
            left_index = index - 1
            left = syntax_subtrees[left_index]
            if left.syntax_type not in FILTER_EXPRESSIONS:
                raise BadExpression('Left operand expected to be a filter expression')

            # Or can only have filters as right operands
            right_index = index + 1
            right = syntax_subtrees[right_index]
            if right.syntax_type not in FILTER_EXPRESSIONS:
                raise BadExpression('Right operand expected to be a filter expression')

            # If there is an incomplete or after this or then an operand is missing
            if right.syntax_type is _SyntaxType.OR_OPERATION and right.left is None:
                raise BadExpression('Missing right operand for or operation')

            # Make the operands children of the operation
            subtree.left = left
            subtree.right = right

            # Replace the separate subtrees with the combined subtree in the 
            # list, adjusting the list index and length as necessary
            syntax_subtrees = syntax_subtrees[:left_index] + [subtree] + syntax_subtrees[right_index + 1:]
            index = left_index
            syntax_subtrees_len = len(syntax_subtrees)

        index += 1

    # All operators have been combined with their operands, which should result in all
    # syntax subtrees being combined. If there is more than one subtree remaining then
    # it means there is a missing operator.
    syntax_subtrees_len = len(syntax_subtrees)
    if syntax_subtrees_len != 1:
        raise BadExpression('Missing operator(s)')

    return syntax_subtrees[0]


def _to_function(syntax_tree, trends):
    try:
        if syntax_tree.syntax_type is _SyntaxType.NOT_OPERATION:
            func = logical_not(_to_function(syntax_tree.right, trends))
        elif syntax_tree.syntax_type is _SyntaxType.AND_OPERATION:
            func = logical_and((_to_function(syntax_tree.left, trends), _to_function(syntax_tree.right, trends)))
        elif syntax_tree.syntax_type is _SyntaxType.OR_OPERATION:
            func = logical_or((_to_function(syntax_tree.left, trends), _to_function(syntax_tree.right, trends)))
        elif syntax_tree.syntax_type is _SyntaxType.ACCEPT_FILTER:
            func = accept
        elif syntax_tree.syntax_type is _SyntaxType.REJECT_FILTER:
            func = reject
        elif syntax_tree.syntax_type is _SyntaxType.MATCH_OPERATION:
            func = create_match_filter(syntax_tree.left.value, syntax_tree.right.value)
        elif syntax_tree.syntax_type is _SyntaxType.ABOVE_OPERATION:
            func = create_above_filter(syntax_tree.left.value, syntax_tree.right.value)
        elif syntax_tree.syntax_type is _SyntaxType.BELOW_OPERATION:
            func = create_below_filter(syntax_tree.left.value, syntax_tree.right.value)
        elif syntax_tree.syntax_type is _SyntaxType.TREND_OPERATION:
            func = create_trend_filter(syntax_tree.left.value, trends.get(syntax_tree.right.value))
        else:
            # Missing handling for a new syntax or incorrect handling of existing syntax if here
            raise BadExpression('Bad syntax')
    except BadOperand as e:
        raise BadExpression('Bad Operand: ' + str(e))
    return func
