from abc import ABC, abstractmethod

class LookupTicker(ABC):

    @abstractmethod
    def lookup_ticker(self, ticker):
        pass
